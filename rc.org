#+TITLE: Moomoo's fvwm configuration
#+PROPERTY: header-args :tangle "~/.fvwm/config"

* Environment configuration
** Path variables
#+BEGIN_SRC conf
SetEnv fvwm_wall $[FVWM_USERDIR]/wallpapers
SetEnv fvwm_icons $[FVWM_USERDIR]/icons
SetEnv fvwm_scpts $[FVWM_USERDIR]/scripts
#+END_SRC

** Default applications
#+BEGIN_SRC conf
SetEnv fvwm_web 'firefox'
SetEnv fvwm_emacs 'emacsclient -nc'
SetEnv fvwm_term 'xterm'
#+END_SRC

** General behavior
#+BEGIN_SRC conf
DefaultFont "xft:M+ 1M:size=10"
EwmhBaseStruts 6 6 6 6
IgnoreModifiers L25
OpaqueMoveSize unlimited
HideGeometryWindow
XorValue 55555
Style "*" SnapAttraction 2 SameType ScreenAll, SnapGrid 8, TileCascadePlacement, Grabfocus, ResizeOpaque, MaxWindowSize $[vp.width]p $[vp.height]p, EWMHUseStackingOrderHints, !UsePPosition, !Icon
#+END_SRC

** Start function
#+BEGIN_SRC conf
DestroyFunc StartFunction
AddToFunc   StartFunction
+ I Exec fvwm-root -r $[HOME]/Imagens/wallpapers/wallhaven-377063.png
+ I Module FvwmButtons
#+END_SRC

** Init function
#+BEGIN_SRC conf :tangle no
DestroyFunc InitFunction
AddToFunc   InitFunction
+ I Exec volumeicon
+ I Exec cbatticon -u 30 -i standard -c "poweroff" -l 15 -r 5
#+END_SRC

* Virtual desktops
#+BEGIN_SRC conf
DesktopSize 4x1
DesktopName 0 VOID
EdgeScroll 0 0
EdgeResistance 0
EdgeThickness 0
Style "*" EdgeMoveDelay 350, EdgeMoveResistance 350
#+END_SRC

* Mouse and focus
** Behavior
#+BEGIN_SRC conf
ClickTime 350
MoveThreshold 3
Style "*" SloppyFocus
#+END_SRC

** Cursor styles
#+BEGIN_SRC conf
CursorStyle root left_ptr
CursorStyle title left_ptr
CursorStyle default left_ptr
CursorStyle position left_ptr
CursorStyle sys left_ptr
CursorStyle menu left_ptr
CursorStyle resize fleur
CursorStyle stroke cross
CursorStyle select draped_box
#+END_SRC

* Functions
** Alt-tab window switching without menu
#+BEGIN_SRC conf
DestroyFunc WinFocus
AddToFunc WinFocus
+ I Iconify off
+ I FlipFocus
+ I Raise
#+END_SRC

** Browse directories on menu
#+BEGIN_SRC conf
AddToFunc FuncFvwmMenuDirectory
+ I PipeRead "fvwm-menu-directory -d '$0' -icon-t 'icons/duser.png' -icon-d 'icons/folder.svg' -icon-a 'icons/dapp.png' -icon-f 'icons/dfiles.png' \
-command-t 'Exec $[fvwm_emacs] "%d"' \
-command-f 'Exec mimi "%f"' "
#+END_SRC

** Manual tiling
#+BEGIN_SRC conf
DestroyFunc TileLeft
AddToFunc TileLeft
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 100
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move +0 +0

DestroyFunc TileRight
AddToFunc TileRight
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 100
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move -0 +0

DestroyFunc TileTop
AddToFunc TileTop
+ I ThisWindow (!Shaded, !Iconic) Maximize 100 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move +0 +0

DestroyFunc TileBottom
AddToFunc TileBottom
+ I ThisWindow (!Shaded, !Iconic) Maximize 100 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move +0 -0

DestroyFunc TileTopLeft
AddToFunc TileTopLeft
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move +0 +0

DestroyFunc TileTopRight
AddToFunc TileTopRight
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move -0 +0

DestroyFunc TileBottomLeft
AddToFunc TileBottomLeft
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move +0 -0

DestroyFunc TileBottomRight
AddToFunc TileBottomRight
+ I ThisWindow (!Shaded, !Iconic) Maximize 50 50
+ I ThisWindow (Maximized, !Shaded, !Iconic) Move -0 -0
#+END_SRC

** Take a scrot
#+BEGIN_SRC conf
DestroyFunc FvwmScrotty
AddToFunc   FvwmScrot
+ I Exec scrot '%Y-%m-%H%M%s-%d_$wx$h.png' -e 'mv $f ~/Imagens/scrots'
#+END_SRC

* Keybindings
** General shortcuts
#+BEGIN_SRC conf
Key q A 4M Restart
Key space A 4 Menu MainMenu
Key space A M WindowList Root NoGeometry
Key r A 4 Exec $[fvwm_scpts]/run-dialog
Key return A 4 Exec $[fvwm_term]
Key print A N FvwmScrot
#+END_SRC

** Window management
#+BEGIN_SRC conf
Key z A 4 Next (CurrentPage, AcceptsFocus) WinFocus
Key w A 4 Close
Key d A 4 Resize
Key s A 4 Move
Key c A 4 Move 50-50w 50-50w
Key f A 4 Maximize Fullscreen
Key m A 4 Maximize 0 100
Key m A 4M Maximize 100 0
Key m A 4C Maximize 100 100
#+END_SRC

** Window tiling
#+BEGIN_SRC conf
Key Left A 4 TileLeft
Key Right A 4 TileRight
Key Up A 4 TileTop
Key Down A 4 TileBottom
Key Left A M TileTopLeft
Key Right A M TileTopRight
Key Up A M TileBottomLeft
Key Down A M TileBottomRight
#+END_SRC

** Edge snapping
#+BEGIN_SRC conf
Key Left A C Move 0 w+0
Key Right A C Move -0 w+0
Key Up A C Move w+0 0
Key Down A C Move w+0 -0
#+END_SRC

** Page management
#+BEGIN_SRC conf
Key 1 A 4 GotoPage 0 0
Key 2 A 4 GotoPage 1 0
Key 3 A 4 GotoPage 2 0
Key 4 A 4 GotoPage 3 0
Key 1 A 4M MovetoPage 0 0
Key 2 A 4M MovetoPage 1 0
Key 3 A 4M MovetoPage 2 0
Key 4 A 4M MovetoPage 3 0
#+END_SRC

* Mousebindings
#+BEGIN_SRC conf
Mouse 1 R A WindowList Root NoGeometry
Mouse 3 R A Menu MainMenu
#+END_SRC

* Fvwm menu
** Create menu
#+BEGIN_SRC conf
DestroyMenu MainMenu
AddToMenu   MainMenu "MAIN MENU" Title
AddToMenu   MainMenu MissingSubmenuFunction FuncFvwmMenuDirectory
+ "%icons/web.png%Web browser" Exec $[fvwm_web]
+ "%icons/emacs.png%Kitchen sink" Exec $[fvwm_emacs]
+ "%icons/term.png%Terminal app" Exec $[fvwm_term]
+ "%icons/run.png%Run dialog" Exec $[fvwm_scpts]/run-dialog
+ "%icons/program.png%Programs" Popup Apps
+ "%icons/browse.png%File browser" Popup $[HOME]
+ "%icons/dotfiles.png%Dotfiles" Popup Dots
+ "" Nop
+ "%icons/manpages.png%Manpages" Exec $[fvwm_web] https://man.voidlinux.org/
+ "%icons/console.png%Console" Module FvwmConsole
+ "%icons/session.png%Quit..."  Module FvwmForm FvwmForm-Quit
#+END_SRC

** Programs popup
#+BEGIN_SRC conf
DestroyMenu Apps
AddToMenu   Apps "applications" Title
+ "%icons/top.png%top" Exec $[fvwm_term] -e top
+ "%icons/gimp.png%gimp" Exec gimp 
+ "%icons/retroarch.png%retroarch" Exec retroarch
+ "%icons/retroarch.png%minecraft" Exec minecraft
+ "%icons/photo.png%sxiv" Exec sxiv ~/Imagens/wallpapers/ -t
+ "%icons/transmission.png%transmission" Exec transmission-gtk
+ "%icons/mpv.png%mpv" Exec mpv
+ "%icons/audio.png%cmus" Exec $[fvwm_term] -e cmus
+ "%icons/lxappearance.png%lxappearance" Exec lxappearance
+ "%icons/sound.png%alsamixer" Exec $[fvwm_term] -e alsamixer
#+END_SRC

** Dotfiles popup
#+BEGIN_SRC conf
DestroyMenu Dots
AddToMenu   Dots "settings" Title
+ "%icons/conf-files.png%fvwm" Exec $[fvwm_emacs] $[FVWM_USERDIR]/rc.org
+ "%icons/conf-files.png%emacs" Exec $[fvwm_emacs] $[HOME]/.emacs.d/cf.org
+ "%icons/conf-files.png%compton" Exec $[fvwm_emacs] $[HOME]/.config/compton.conf
+ "%icons/conf-files.png%xdefaults" Exec $[fvwm_emacs] $[HOME]/.Xdefaults
+ "%icons/conf-files.png%xinit" Exec $[fvwm_emacs] $[HOME]/.xinitrc
+ "%icons/conf-files.png%bashrc" Exec $[fvwm_emacs] $[HOME]/.bashrc
+ "%icons/conf-files.png%profile" Exec $[fvwm_emacs] $[HOME]/.bash_profile
#+END_SRC

* Colorsets
#+BEGIN_SRC conf
Colorset 0 fg #c6c6c6, bg #303030, sh #8e294e, hi #ba3645, Plain, Noshape, Tint, Alpha
Colorset 1 bg #ba3645, hi #ba3645, sh #ba3645, fgsh, Plain, NoShape, Tint, Alpha
Colorset 2 bg #303030, hi #303030, sh #303030, fgsh, Plain, NoShape, Tint, Alpha
Colorset 3 bg #3A3A3A, hi #3A3A3A, sh #3A3A3A, fgsh, Plain, NoShape, Tint, Alpha
Colorset 4 bg #3A3A3A, hi #5a9bbd, sh #5a9bbd, fgsh, Plain, NoShape, Tint, Alpha
Colorset 5 bg #3A3A3A, hi #ba3645, sh #ba3645, fgsh, Plain, NoShape, Tint, Alpha
#+END_SRC

* Styling
** Window decor
#+BEGIN_SRC conf
DestroyDecor WinDecor
AddToDecor   WinDecor
+ TitleStyle Centered Height 5
+ TitleStyle -- Flat
Style "*" TitleAtLeft
#+END_SRC

** Window style
#+BEGIN_SRC conf
Style "*" UseDecor WinDecor
Style "*" BorderWidth 0, HandleWidth 0
Style "*" HilightFore #ba3645, HilightBack #ba3645
Style "*" ForeColor #5a9bbd, BackColor #5a9bbd
Style "*" DecorateTransient, StackTransientParent, !FPGrabFocusTransient, FPReleaseFocusTransient
#+END_SRC

** Menu style
#+BEGIN_SRC conf
MenuStyle "*" PopupDelayed, PopupDelay 200, PopdownImmediately
MenuStyle "*" PopupOffset 2 100, BusyCursor, TitleWarpOff
MenuStyle "*" TitleUnderlines2, SeparatorsShort, TrianglesRelief
MenuStyle "*" PopupAsSubmenu, HldSubmenus, SubmenusRight
MenuStyle "*" AutomaticHotkeys, ItemFormat "%|%5.5i%1.3l%2.3>%|"
MenuStyle "*" VerticalItemSpacing 1 1, VerticalTitleSpacing 2 2, VerticalMargins 2 2, MenuColorset 0
MenuStyle "*" BorderWidth 2, Hilight3DOff, ActiveFore #afd7ff
#+END_SRC

** Program styles
#+BEGIN_SRC conf
Style "Firefox" InitialMapCommand Maximize Fullscreen, StartsOnPage 0 0
Style "Emacs" PositionPlacement Center, StartsOnPage 1 0
Style "xterm" PositionPlacement Center, StartsOnPage 2 0
Style "Gimp" StartsOnPage 3 0
#+END_SRC

* Modules
** FvwmPager
#+BEGIN_SRC conf
DestroyModuleConfig FvwmPager: *
*FvwmPager: Font none
*FvwmPager: UseSkipList
*FvwmPager: SolidSeparators
*FvwmPager: NoDeskHilight
*FvwmPager: Back #3A3A3A
*FvwmPager: Fore #303030
*FvwmPager: Hilight #3A3A3A
*FvwmPager: WindowColorsets 4 5
*FvwmPager: WindowBorderWidth 1
*FvwmPager: Window3dBorders
*FvwmPager: Balloons All
*FvwmPager: BallonYOffset +2
*FvwmPager: BalloonFont "xft:M+ 1m:size=10"
*FvwmPager: BalloonBack #3A3A3A
*FvwmPager: BalloonFore #c6c6c6
*FvwmPager: BalloonBorderColor #303030
#+END_SRC

** FvwmButtons
#+BEGIN_SRC conf
DestroyModuleConfig FvwmButtons: *
Style "FvwmButtons" !Title, !Handles, !Borders, !Iconifiable, !Maximizable, Sticky, WindowListSkip, Neverfocus, CirculateSkip, FixedPosition, FixedSize, Layer 1
*FvwmButtons: Geometry 150x100+6+6
*FvwmButtons: BoxSize fixed
*FvwmButtons: Rows 3, Columns 0
*FvwmButtons: (Swallow(NoClose,UseOld) 'Time' 'Module FvwmScript $[fvwm_scpts]/Time', Colorset 1, Action(Mouse 1) "Exec gsimplecal)
*FvwmButtons: (Swallow(NoClose,UseOld) 'Bat' 'Module FvwmScript $[fvwm_scpts]/Bat', Colorset 2)
*FvwmButtons: (Swallow(NoClose,UseOld) 'FvwmPager' 'FvwmPager 0 0', Colorset 3)
#+END_SRC

** FvwmForm
*** Quit dialog
#+BEGIN_SRC conf
DestroyModuleConfig FvwmForm-Quit: *
*FvwmForm-Quit: Font 'xft:M+ 1m:size=10'
*FvwmForm-Quit: ButtonFont 'xft:M+ 1m:size=10'
*FvwmForm-Quit: Fore #c6c6c6
*FvwmForm-Quit: Back #303030
*FvwmForm-Quit: ItemFore #c6c6c6
*FvwmForm-Quit: ItemBack #3A3A3A
*FvwmForm-Quit: GrabServer
*FvwmForm-Quit: WarpPointer
*FvwmForm-Quit: PadVText 10
*FvwmForm-Quit: Command Beep
*FvwmForm-Quit: Text "Please, select operation:"
*FvwmForm-Quit: Line center
*FvwmForm-Quit: Button quit "Shutdown" ^S
*FvwmForm-Quit: Command Exec sudo poweroff
*FvwmForm-Quit: Button quit "Reboot" ^R
*FvwmForm-Quit: Command Exec sudo reboot
*FvwmForm-Quit: Button quit "Suspend" ^Z
*FvwmForm-Quit: Command Exec sudo zzz & slock
*FvwmForm-Quit: Button quit "Logout" ^L
*FvwmForm-Quit: Command Quit
*FvwmForm-Quit: Button quit "Cancel" ^[
*FvwmForm-Quit: Command Nop
#+END_SRC

*** Calendar
