" moomoo's vim config

" filetype support
filetype plugin indent on

" minimal automatic indenting for any filetype
set autoindent

" proper backspace behavior
set backspace=indent,eol,start

" don't redraw while executing macros
set lazyredraw

" syntax highlight
syntax on

" sets more than one unsaved buffers
set hidden

" incremental search
set incsearch 

" shows current line 
set ruler

" command-line completion
set wildmenu
set wildmode=full
set wildcharm=<C-z>

" juggling with files
nnoremap ,f :find *
nnoremap ,s :sfind *
nnoremap ,v :vert sfind *
nnoremap ,t :tabfind *

" statusline
set laststatus=2
hi StatusLine ctermfg=NONE ctermbg=red cterm=NONE
hi StatusLineNC ctermfg=black ctermbg=red cterm=NONE
hi User1 ctermfg=black ctermbg=magenta
hi User2 ctermfg=NONE ctermbg=NONE
hi User3 ctermfg=black ctermbg=blue
hi User4 ctermfg=black ctermbg=cyan
set statusline=\                    " Padding
set statusline+=%f                  " Path to the file
set statusline+=\ %1*\              " Padding & switch colour
set statusline+=%y                  " File type
set statusline+=\ %2*\              " Padding & switch colour
set statusline+=%=                  " Switch to right-side
set statusline+=\ %3*\              " Padding & switch colour
set statusline+=line                " of Text
set statusline+=\                   " Padding
set statusline+=%l                  " Current line
set statusline+=\ %4*\              " Padding & switch colour
set statusline+=of                  " of Text
set statusline+=\                   " Padding
set statusline+=%L                  " Total line
set statusline+=\                   " Padding

" disable some bundled vim plugins
let g:loaded_netrwPlugin = 1
let g:loaded_vimballPlugin = 1
let g:loaded_tarPlugin = 1
let g:loaded_zipPlugin = 1
let g:loaded_rrhelper = 1
let g:loaded_gzip = 1
let g:loaded_getscriptPlugin = 1
let g:loaded_logipat = 1

" prevent .viminfo file from being created
let skip_defaults_vim=1
set viminfo=""
